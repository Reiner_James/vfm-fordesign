<?php
/**
* Additional user custom fields
* EXAMPLE
* rename this file in "customfields.php" and adjust the attributes
*/
$customfields = array(
    'fullname' => array(      // Attribute name
        'name' => 'Full Name', // Label text
        'type' => 'text',    // input type
    ),
    'phone' => array(
        'name' => 'Phone',
        'type' => 'text',
    ),

    'occupation' => array(
        'name' => 'Occupation',
        'type' => 'text',
    ),

    'address' => array(
        'name' => 'Address',
        'type' => 'textarea',
    ),

    'notes' => array(
        'name' => 'Notes',
        'type' => 'textarea',
    ),
);

/**
* use this code inside template parts
* Print 'text-example' attribute from specific user 'jondoe'.
*/
// $getuser = Utils::getCurrentUser('jondoe'); 
// if (isset($getuser['text-example'])){
//     echo $getuser['text-example'];
// }
/**
* Print 'text-example' attribute from current logged user.
*/
// echo GateKeeper::getUserInfo('text-example'); 