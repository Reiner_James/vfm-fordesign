<?php

 $_USERS = array (
  0 => 
  array (
    'name' => 'admin',
    'pass' => '$1$TAxtRk08$76qKq7EqdLYa8YnsNX9bg0',
    'role' => 'superadmin',
  ),
  4 => 
  array (
    'name' => 'jims',
    'pass' => '$1$B4PXxqFZ$YSYU/OD.wZLdTdqk6Pu24/',
    'role' => 'admin',
    'dir' => '["jims"]',
    'email' => 'rico.freelancer@gmail.com',
    'fullname' => 'Jims Rico',
    'phone' => '09467331651',
    'occupation' => 'Computer Programmer',
    'address' => 'Legazpi City',
    'notes' => 'Freelance Developer',
  ),
);
