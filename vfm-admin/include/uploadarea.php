<?php
/**
 * VFM - veno file manager: include/uploadarea.php
 *
 * PHP version >= 5.3
 *
 * @category  PHP
 * @package   VenoFileManager
 * @author    Nicola Franchini <support@veno.it>
 * @copyright 2013 Nicola Franchini
 * @license   Exclusively sold on CodeCanyon
 * @link      http://filemanager.veno.it/
 */
if (!defined('VFM_APP')) {
    return;
}
/**
* UPLOAD AREA
*/
if ($location->editAllowed() 
    && ($gateKeeper->isAllowed('upload_enable') || $gateKeeper->isAllowed('newdir_enable'))
) { ?>
    <section class="vfmblock uploadarea">
    <?php
    $post_url = Utils::removeQS($_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'], array('response'));
    /**
    * Upload files
    */
    if ($gateKeeper->isAllowed('upload_enable')) { 

        if ($gateKeeper->isAllowed('newdir_enable')) { 
            $upload_class = "span-6";
        } else {
            $upload_class = "intero";
        } ?>
        <form enctype="multipart/form-data" method="post" id="upForm" action="<?php echo htmlspecialchars($post_url);?>">
            <input type="hidden" name="location" value="<?php echo $location->getDir(true, false, false, 0); ?>">
            <div id="upload_container" class="input-group pull-left <?php echo $upload_class; ?>">
                <span class="input-group-addon ie_hidden">
                    <i class="fa fa-files-o fa-fw"></i>
                </span>
                <span class="input-group-btn" id="upload_file">
                    <span class="upfile btn btn-primary btn-file">
                        <i class="fa fa-files-o fa-fw"></i>
                        <input name="userfile[]" type="file" class="upload_file" multiple />
                    </span>
                </span>
                <input class="form-control" type="text" readonly name="fileToUpload" id="fileToUpload" placeholder="<?php echo $setUp->getString("browse"); ?>">
                <span class="input-group-btn">
                    <button class="upload_sumbit btn btn-primary" type="submit" id="upformsubmit" disabled>
                        <i class="fa fa-upload fa-fw"></i>
                    </button>
                    <a href="javascript:void(0)" class="btn btn-primary" id="upchunk">
                        <i class="fa fa-upload fa-fw"></i>
                    </a>
                </span>
            </div>
        </form>
        <script type="text/javascript" src="vfm-admin/js/uploaders.js?v=<?php echo $vfm_version; ?>"></script>
        <?php
        $useragent = strtolower($_SERVER['HTTP_USER_AGENT']);
        $android = (stripos($useragent, 'android') !== false ? 'yes' : 'no');
        $singleprogress = ($setUp->getConfig('single_progress') ? true : 0);
        ?>
        <script type="text/javascript">
        $(document).ready(function(){
            resumableJsSetup(
                "<?php echo $android; ?>", 
                "<?php echo urlencode($location->getFullPath()); ?>&logloc=<?php echo urlencode($location->getDir(true, false, false, 0)); ?>", 
                "<?php echo $setUp->getString('browse'); ?>",
                <?php echo $singleprogress; ?>,
                <?php echo $setUp->getChunkSize(); ?>
            );
        });
        </script>
        <?php
    }
    /**
    * Create directory
    */
    if ($gateKeeper->isAllowed('newdir_enable')) { 
        if ($gateKeeper->isAllowed('upload_enable')) { 
            $newdir_class = "span-6";
        } else {
            $newdir_class = "intero";
        } ?>
        <form enctype="multipart/form-data" method="post" action="<?php echo htmlspecialchars($post_url);?>">
            <div id="newdir_container" class="input-group pull-right <?php echo $newdir_class; ?>">
                <span class="input-group-btn">
                <a href="javascript:void(0)" class="btn btn-primary" id="upchunkSingleFile">  <!--TODO-->
                       Upload Folder
                    </a>
                    <button class="btn btn-success pull-right" type="button" onclick="toggleEventModal();">
                       New Event
                    </button>
                </span>
                
            </div>
        </form>
        <script type="text/javascript">
        $(document).ready(function(){
            <?php if(count(explode("/",$location->getDir(true, false, false, 0))) == 4
            && $gateKeeper->getUserInfo('role') == 'admin'){?>;
            $("#neweventpanel").modal('toggle');
            <?php }?>

            $("#userdir").keypress(function(e) {
                var k = e.keyCode,
                        $return = ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32  || (k >= 48 && k <= 57));
                if(!$return) {
                    return false;
                }
                
            });

            $("#othercategory").keypress(function(e) {
                var k = e.keyCode,
                        $return = ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32  || (k >= 48 && k <= 57));
                if(!$return) {
                    return false;
                }
                
            });
           
        });
        const selectCategory = (category) =>{
           if(category == "Others"){
               $("#divEventOthers").css("display","block");
               $("#othercategory").attr('required', true);
           }else{
            $("#divEventOthers").css("display","none");
            $("#othercategory").removeAttr('required');
           }
        };

        const toggleEventModal = () =>{
            $("#neweventpanel").modal('toggle');
        };

        </script>
        <?php
    }

    if ($gateKeeper->isAllowed('upload_enable')
        && strlen($setUp->getConfig('preloader')) > 0
    ) {
        // upload progress bar
        $percentage_class = $setUp->getConfig('show_percentage') ? ' fullp' : ''; ?>
        <div class="intero<?php echo $percentage_class;?>">
            <div class="progress progress-striped active" id="progress-up">
                <div class="upbar progress-bar <?php echo $setUp->getConfig('progress_color'); ?>" 
                    role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                    <p class="pull-left propercent"></p>
                </div>
            </div>
        <?php
        // second progress bar for individual files
        if ($setUp->getConfig('single_progress')) { ?>
            <div class="progress progress-single" id="progress-up-single">
                <div class="upbarfile progress-bar <?php echo $setUp->getConfig('progress_color'); ?>" 
                    role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                    <p class="pull-left propercent"></p>
                </div>
            </div>
            <?php
        } ?>
        </div>
        <?php
    } ?>
    </section>

    <div class="modal fade" tabindex="-1" role="dialog" id="neweventpanel">
    <div class="modal-dialog">
    <div class="modal-content">
    <form enctype="multipart/form-data" method="post" action="<?php echo htmlspecialchars($post_url);?>">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <i class="fa fa fa-file-video-o"></i> New Event
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">

                    <div class="row">
                        <div class="col-md-6 form-group">
                             <label>
                                Event Category
                            </label>
                            <div class="input-group col-xs-12">
                            <select name="eventcategory" class="form-control coolselect" onchange="selectCategory(this.value)" required>
                                <option>Weddings</option>
                                <option value="B nai Mitzvah">B'nai Mitzvah </option>
                                <option value="Bar Mitzvah Readings" >Bar-Mitzvah Readings</option>
                                <option value="Bar Mitzvah Parties">Bar-Mitzvah Parties</option>
                                <option value="Bar Mitzvah Reading + Parties">Bar-Mitzvah Reading + Parties</option>
                                <option value="Bat Mitzvahs">Bat-Mitzvahs</option>
                                <option>Engagement Parties</option>
                                <option>Birthday Parties</option>
                                <option>Others</option>
                            </select>
                            </div>
                        </div>
                        <div class="col-md-6 form-group" style="display:none;" id="divEventOthers">
                             <label>
                                Other, Specify
                            </label>
                            <div class="input-group col-xs-12">
                            <input type="text" name="othercategory" class="form-control" id="othercategory" 
                                    placeholder="Others, specify">
                            </div>
                        </div>
                    </div> <!-- row -->
                    <div class="row">
                        <div class="col-md-6 form-group">
                             <label>
                                Event Name
                            </label>
                            <div class="input-group col-xs-12">
                                    <input type="text" name="userdir" class="form-control" id="userdir" 
                                    placeholder="Event Name" required>
                            </div>
                        </div>
                    </div> <!-- row -->

                    <div class="row">
                        <div class="col-md-6 form-group">
                             <label>
                                Event Date
                            </label>
                            <div class="input-group">
                                    <input type="date" name="eventdate" 
                                    value="<?php echo date('Y-m-d'); ?>" class="form-control" 
                                    id="eventdate" required>
                            </div>
                        </div>
                    </div> <!-- row -->

                    <div class="row">
                        <div class="col-md-6 form-group">
                             <label>
                                Event Details
                            </label>
                            <div class="input-group">
                                    <textarea cols="70" 
                                    name="eventdetails" id="eventdetails" rows="8" required></textarea>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div>
            </div><!-- /.modal-body -->
            <div class="modal-footer">
                <button class="btn btn-success btn-lg">
                    <i class="fa fa-arrow-right"></i> 
                    Next
                </button>
            </div>
        </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
}
