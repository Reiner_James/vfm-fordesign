<?php

$redirectUrl = $_COOKIE['redirectUrl'];

require 'vfm-admin/config.php';
require_once 'vfm-admin/class/class.setup.php';
require_once 'vfm-admin/class/class.utils.php';
require_once 'vfm-admin/class/class.gatekeeper.php';

$setUp = new SetUp();
$lang = $setUp->lang;
require "vfm-admin/translations/".$lang.".php";

use PHPMailer\PHPMailer\PHPMailer;
require_once 'vfm-admin/mail/vendor/autoload.php';

$setfrom = SetUp::getConfig('email_from');
$appurl =  SetUp::getConfig('script_url');
$mail = new PHPMailer();

$mail->CharSet = 'UTF-8';
$mail->setLanguage($lang);
$companyEmail = "jimelitor@yahoo.com";
$emailCC = "thrivecarenetwork@gmail.com";


if (SetUp::getConfig('smtp_enable') == true) {

    $mail->isSMTP();
    $mail->SMTPDebug = (SetUp::getConfig('debug_smtp') ? 2 : 0);
    $mail->Debugoutput = 'html';

    $smtp_auth = SetUp::getConfig('smtp_auth');

    $mail->Host = SetUp::getConfig('smtp_server');
    $mail->Port = (int)SetUp::getConfig('port');
    if (version_compare(PHP_VERSION, '5.6.0', '>=')) {
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true,
            )
        );
    }
    if (SetUp::getConfig('secure_conn') !== "none") {
        $mail->SMTPSecure = SetUp::getConfig('secure_conn');
    }
    
    $mail->SMTPAuth = $smtp_auth;

    if ($smtp_auth == true) {
        $mail->Username = SetUp::getConfig('email_login');
        $mail->Password = SetUp::getConfig('email_pass');
    }
}
$mail->setFrom($setfrom, SetUp::getConfig('appname'));
//$mail->addAddress($postmail, '<'.$postmail.'>');
$mail->addAddress($companyEmail);
$mail->AddCC($emailCC);

$mail->Subject = SetUp::getConfig('appname').": New Event Uploaded";

//$altmessage = $setUp->getString('follow_this_link_to_activate')."/n".$activationlink;

$email_logo = SetUp::getConfig('email_logo', false) ? 'vfm-admin/_content/uploads/'.SetUp::getConfig('email_logo') : '../images/px.png';;
$mail->AddEmbeddedImage($email_logo, 'logoimg');

// Retrieve the email template required
$message = file_get_contents('vfm-admin/_content/mail-template/template-upload-notification.html');

// Replace the % with the actual information
$message = str_replace('%app_url%', $appurl, $message);
$message = str_replace('%app_name%', SetUp::getConfig('appname'), $message);

$message = str_replace(
    '%fullname%', 
    str_replace("+", " ", $_COOKIE['client']), $message
);

$message = str_replace(
    '%eventcategory%', 
    str_replace("+", " ", $_COOKIE['eventcategory']), $message
);

$message = str_replace(
    '%eventname%', 
    str_replace("+"," ",$_COOKIE['eventname']), $message
);

$message = str_replace(
    '%eventdate%', 
    $_COOKIE['eventdate'], $message
);

$message = str_replace(
    '%eventdetails%', 
    str_replace("+"," ",$_COOKIE['eventdetails']), $message
);

$message = str_replace(
    '%eventlink%', 
    $appurl."?dir=".substr($_COOKIE['eventlink'],2), $message
);

$mail->msgHTML($message);

//$mail->AltBody = $altmessage;

if (!$mail->send()) {
    echo '<div class="alert alert-danger" role="alert">Mailer Error: ' .$mail->ErrorInfo.'</div>';
}else{
    header("Location: $redirectUrl"); 
} 
exit;